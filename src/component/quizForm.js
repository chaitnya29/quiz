import React from 'react';

var quizForm = (props) => {
    return(
        <form onSubmit={props.onSubmit}>
            <input type="text" name="question" value={props.question} onChange={props.handleQuestionChange} placeholder="Enter question here !"/><br />
            <input type="text" name="answer" value={props.answer} onChange={props.handleAnswerChange} placeholder="Enter answer here."/><br />
            <input type="submit" value="submit"/><br />
        </form>
    );
}

export default quizForm;