import React from 'react';
import ReactDOM from 'react-dom';
import QuizForm from './component/quizForm';

class Quiz extends React.Component{
    constructor(){
        super();
        this.state = {
            question : '',
            answer : '',
        }
        this.onsubmit=this.onsubmit.bind(this);
        this.handleQuestionChange=this.handleQuestionChange.bind(this);
        this.handleAnswerChange=this.handleAnswerChange.bind(this);
    }
    onsubmit(evt){
        alert(this.state.answer);
        evt.preventDefault();
    }
    handleQuestionChange(evt){
        this.setState({
            question : evt.target.value,
        });
    }
    handleAnswerChange(evt){
        this.setState({
            answer : evt.target.value,
        });
    }
    render(){
        return(
            <QuizForm
                question={this.state.question}
                answer={this.state.answer}
                handleQuestionChange={this.handleQuestionChange}
                handleAnswerChange={this.handleAnswerChange}
                onSubmit={this.onsubmit}
                
                 />
        );
    }
}

ReactDOM.render(<Quiz />, document.getElementById('root'));